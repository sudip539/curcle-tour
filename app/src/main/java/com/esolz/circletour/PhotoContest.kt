package com.esolz.circletour

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_photo_contest.*
import kotlinx.android.synthetic.main.activity_photo_contest.toolbar5

class PhotoContest : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_contest)
        toolbar5.setNavigationIcon(R.drawable.ic_baseline_keyboard_backspace_24)
        toolbar5.setNavigationOnClickListener {
            onBackPressed()

        }
    }
}